# Code written by Oladeji Sanayolu (17/7/2021)
# Followed The Flask Mega-Tutorial by miguelgrinberg.com

from app import create_app, db, cli
from app.models import User, Post, Notification, Message, Task

app = create_app()
cli.register(app)

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post, 'Message': Message, 'Notification': Notification, 'Task': Task}
